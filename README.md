# Krycí jména
Překlad jedoduchého online provedení hry [Krycí jména](http://krycijmena.cz/)

Online ke hraní česky: https://clon.gitlab.io/codenames/ původní anglický projekt: http://sjlevin.github.io/codenames/ ([repozitář](https://github.com/sjlevin/codenames))

---
# Codenames
A simple attempt to reproduce the game board for Codenames

The game of original project can be played here: http://sjlevin.github.io/codenames/
